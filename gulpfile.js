var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync')
    rigger = require('gulp-rigger')

gulp.task('html:build', function () {
    gulp.src('app/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('sass', function(){
    return gulp.src(['dist/sass/main.scss'])
        .pipe(sass({
          includePaths: require('node-normalize-scss').includePaths
        }))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(concat('styles.min.css'))
        .pipe(cssnano())
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function(){
  return gulp.src([
    'dist/js/scripts.js',
  ])
  .pipe(gulp.dest('dist/js'))
  .pipe(browserSync.reload({stream: true}));
});

gulp.task('scripts-libs', function(){
  return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/slick-carousel/slick/slick.min.js',
    'node_modules/swiper/dist/js/swiper.min.js',
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
    'node_modules/overlayscrollbars/js/jquery.overlayScrollbars.min.js'
  ])
  .pipe(concat('libs.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('dist/js'));
});

gulp.task('css-libs', ['sass'], function(){
  return gulp.src([
    'node_modules/normalize.css/normalize.css',
    'node_modules/slick-carousel/slick/slick.css',
    'node_modules/swiper/dist/css/swiper.min.css',
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css',
    'node_modules/overlayscrollbars/css/OverlayScrollbars.min.css'
  ])
  .pipe(concat('libs.css'))
  .pipe(cssnano())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('dist/css'));
});

gulp.task('browser-sync', function(){
    browserSync({
      server: {
        baseDir: 'dist',
        index: "card.html"
      },
      notify: false
    });
});

gulp.task('watch', ['browser-sync', 'html:build', 'css-libs', 'scripts', 'scripts-libs'], function(){
    gulp.watch('dist/sass/**/*.scss', ['sass']);
    gulp.watch('app/templates/*.html', ['html:build'], browserSync.reload);
    gulp.watch('app/*.html',['html:build'], browserSync.reload);
    gulp.watch('dist/*.html', browserSync.reload);
    gulp.watch('dist/js/*.js', ['scripts']);
});
