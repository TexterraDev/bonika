$(document).ready(function(){
	$('.subscribe-form button[type="submit"]').click(function(event) {
		event.preventDefault();
		$('.overlay').fadeIn();
		$('.popup-reminder').fadeIn();
	});

	$('.callback-form button[type="submit"]').click(function(event) {
		$(this).parents('.popup').fadeOut();
		event.preventDefault();
		$('.overlay').fadeIn();
		$('.popup-reminder').fadeIn();
	});
});