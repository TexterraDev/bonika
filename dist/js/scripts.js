$(document).ready(function(){

  $(window).scroll(function(){
      if ($(this).scrollTop() > 550)  {
        $('.scrolltop').addClass('active');
      } else {
        $('.scrolltop').removeClass('active');
      }
  });

  $(window).bind('scroll.once', function () {
    if ($(window).scrollTop() > 60) {
      $('.header-allproducts-mob-menu-btn').addClass('positioned');
    } else {
      $('.header-allproducts-mob-menu-btn').removeClass('positioned');
    }
  });

  $('.scrolltop').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });

	$('.header-allproducts-menu-btn').click(function() {
		$(this).toggleClass('active');
		$('.header-allproducts-menu').slideToggle(210);
	});

    $('.header-allproducts-mob-menu-btn').click(function() {
        $(this).toggleClass('active');
        $('.header-allproducts-menu-block').toggleClass('active');
    });

    if((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
    	$('.header-popular-menu-wrap').hide();
    	$('.header-popular-mob-menu-wrap').show();

    	var swiper = new Swiper('.header-popular-mob-menu-wrap', {
	        direction: 'horizontal',
	        slidesPerView: 'auto',
	        freeMode: true,
	        scrollbar: {
	          el: '.swiper-scrollbar',
	        },
	        mousewheel: true,
	    });

        $('.menu-arrow').click(function() {

            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).siblings('.header-allproducts-menu-submenu').slideUp();
            } else {
                $(this).addClass('active');
                $(this).siblings('.header-allproducts-menu-submenu').slideDown();
            }
        });
        
        
    } else {
    	$('.header-popular-menu-wrap').show();
    	$('.header-popular-mob-menu-wrap').hide();

        $( ".header-allproducts-menu-item" ).hover(
          function() {
            $(this).children('.header-allproducts-menu-submenu').addClass('open');
          }, function() {
            $(this).children('.header-allproducts-menu-submenu').removeClass('open');
          }
        );
    }

    var swiper = new Swiper('.main-banner-slider-wrap', {
        slidesPerView: 1,
        navigation: {
		nextEl: '.main-banner-slider-wrap .swiper-button-next',
		prevEl: '.main-banner-slider-wrap .swiper-button-prev'
			},
    });

    var swiper = new Swiper('.main-popular-products .products-slider-wrap', {
        slidesPerView: 4,
        navigation: {
		nextEl: '.main-popular-products .swiper-button-next',
		prevEl: '.main-popular-products .swiper-button-prev'
            },
        breakpoints: {
            860: {
              slidesPerView: 3
            },
            640: {
              slidesPerView: 2
            },
            480: {
              slidesPerView: 1
            }
        }
    });

    var swiper = new Swiper('.main-new-products .products-slider-wrap', {
        slidesPerView: 4,
        navigation: {
        nextEl: '.main-new-products .swiper-button-next',
        prevEl: '.main-new-products .swiper-button-prev'
            },
        breakpoints: {
            860: {
              slidesPerView: 3
            },
            640: {
              slidesPerView: 2
            },
            480: {
              slidesPerView: 1
            }
        }
    });

    var swiper = new Swiper('.recommended-products .products-slider-wrap', {
        slidesPerView: 4,
        navigation: {
        nextEl: '.recommended-products .swiper-button-next',
        prevEl: '.recommended-products .swiper-button-prev'
            },
        breakpoints: {
            860: {
              slidesPerView: 3
            },
            640: {
              slidesPerView: 2
            },
            480: {
              slidesPerView: 1
            }
        }
    });

    var swiper = new Swiper('.brands-slider-wrap', {
        slidesPerView: 6,
        spaceBetween: 0,
        navigation: {
		nextEl: '.brands-slider-container .swiper-button-next',
		prevEl: '.brands-slider-container .swiper-button-prev'
			},
        breakpoints: {
            910: {
              slidesPerView: 5
            },
            820: {
              slidesPerView: 4
            },
            767: {
              slidesPerView: 3
            },
            640: {
              slidesPerView: 2
            },
            480: {
              slidesPerView: 1
            }
        }
    });

    var swiper = new Swiper('.publications-slider-wrap', {
        slidesPerView: 4,
        touchRatio: 0,
        navigation: {
		nextEl: '.publications-slider-container .swiper-button-next',
		prevEl: '.publications-slider-container .swiper-button-prev'
			},
        breakpoints: {
            910: {
              slidesPerView: 3
            },
            740: {
              slidesPerView: 2
            },
            540: {
              slidesPerView: 1
            }
        }
    });

    var swiper = new Swiper('.article-products-slider-wrap', {
        slidesPerView: 1,
        touchRatio: 0,
        navigation: {
        nextEl: '.article-products-slider-wrap .swiper-button-next',
        prevEl: '.article-products-slider-wrap .swiper-button-prev'
      },
        breakpoints: {
            767: {
              slidesPerView: 3
            },
            567: {
              slidesPerView: 2
            },
            480: {
              slidesPerView: 1
            }
        }
    });

     $('.card-main-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.card-nav-slider'
    });

    $('.card-nav-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      vertical: true,
      asNavFor: '.card-main-slider',
      arrows: true,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            vertical: false,
            centerMode: true,
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }
      ]
    });


    $('input, textarea').focus(function () {
        if ($(this).parents('label').hasClass('input-error') || $(this).parents('label').hasClass('input-error-pass') || $(this).parents('label').hasClass('pass-not-match')) {
            $(this).parents('label').removeClass('input-error');
            $(this).parents('label').removeClass('input-error-pass');
            $(this).parents('label').removeClass('pass-not-match');
        }
    });

    $('.overlay').click(function() {
        $('.popup').fadeOut();
        $(this).fadeOut();
    });

    $('.get-callback-popup').click(function() {
        $('.overlay').fadeIn();
        $('.popup-callback').fadeIn();
    });

    $('.get-autorisation-popup').click(function() {
        $('.overlay').fadeIn();
        $('.popup-autorisation').fadeIn();
    });

    $('.get-registration-popup').click(function() {
        $('.overlay').fadeIn();
        $('.popup-registration').fadeIn();
    });

    $('.get-add-to-cart-popup').click(function() {
        $('.popup-add-to-cart').fadeIn();
    });

    $('.get-conf-policy-popup').click(function(event) {
        event.preventDefault();
        $('.overlay').fadeIn();
        $('.popup-conf-policy').fadeIn();
    });

    $('.get-personal-data-popup').click(function(event) {
        event.preventDefault();
        $('.overlay').fadeIn();
        $('.popup-personal-data').fadeIn();

        if ($(this).hasClass('custom')) {
          $('.popup-personal-data').addClass('custom');
        }
    });

    $('.popup-close').click(function() {
        $(this).parents('.popup').fadeOut();

        if (!$(this).parents('.popup').hasClass('custom')) {
          $('.overlay').fadeOut();
        } else {
          $(this).parents('.popup').removeClass('custom');
        };
    });

    $('.input-spinner button').on('click', function(){
        let input = $(this).closest('.input-spinner').find('input.form-control');

        if($(this).data('action') === 'increment') {
            if(input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                input.val(parseInt(input.val(), 10) + 1);
            }
        } else if($(this).data('action') === 'decrement') {
            if(input.attr('min') === undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
                input.val(parseInt(input.val(), 10) - 1);
            }
        }
    });

    tabSwitch = $('.tabs-switch.active').attr('data-tab');
    $('.tab[data-tab="'+tabSwitch+'"]').show();
    $('.tab').not('[data-tab="'+tabSwitch+'"]').hide();

    $('.tabs-switch').click(function() {
        var tabData = $(this).attr('data-tab');

        $(this).addClass('active');
        $(this).siblings().removeClass('active');

        $('.tab[data-tab="'+tabData+'"]').show();
        $('.tab').not('[data-tab="'+tabData+'"]').hide();
    });

    $('.card-tabs-heading-switch').click(function() {
        if ($(window).width() < 768) {
            $('html, body').animate({
          scrollTop: $('.card-tabs-content-wrap').offset().top 
          }, 500);
          return false;
        }
    });

  // $('.card-product-info-stock').each(function() {
  //   if ($(this).hasClass('empty')) {
  //       $(this).text('Нет в наличии');
  //  } else {
  //       $(this).text('В наличии на складе');
  //  }
  // });

    $('.btn-add-to-favorites').click(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('.btn-add-to-favorites-title').text('В избранное');
        } else {
            $(this).addClass('active');
            $(this).find('.btn-add-to-favorites-title').text('Добавлено в избранное');
        }
    });

    $('.category-sortline-switch:not(.category-sortline-sort-switch)').click(function() {
      if ($(this).hasClass('active')) {
          $(this).removeClass('active');
      } else {
          $(this).addClass('active');
          $(this).siblings().removeClass('active');
      };
    });

    $('.category-sortline-sort-switch').click(function() {
      if ($(this).hasClass('active')) {

        if ($(this).hasClass('down')) {
            $(this).removeClass('down').addClass('up');
        } else {
            $(this).removeClass('up').addClass('down');
        }

      } else {

        $(this).addClass('active down');
        $(this).removeClass('up');
        $(this).siblings().removeClass('active');
      }
    });

    if ($('.category-sortline-view-switch.active').hasClass('lines')) {
        $('.category-items-wrap .product-item-wrap').removeClass('boxes').addClass('lines');
    } else {
        $('.category-items-wrap .product-item-wrap').removeClass('lines').addClass('boxes');
    };

    $('.category-sortline-view-switch').click(function() {
        if ($(this).hasClass('lines')) {
            $('.category-items-wrap .product-item-wrap').removeClass('boxes').addClass('lines');
        } else {
            $('.category-items-wrap .product-item-wrap').removeClass('lines').addClass('boxes');
        }
    });

    $('.category-subcategory-links-heading').click(function() {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $('.category-subcategory-links-wrap').slideUp(250);
        } else {
            $(this).addClass('open');
            $('.category-subcategory-links-wrap').slideDown(250);
        }
    });

    $('.catalog-item-heading-arrow').click(function() {
      if ($(window).width() < 577) {
        if ($(this).parents('.catalog-item').hasClass('open')) {
            $(this).parents('.catalog-item').removeClass('open');
            $(this).parents('.catalog-item-heading').siblings('.catalog-item-content').slideUp(250);
        } else {
            $(this).parents('.catalog-item').addClass('open');
            $(this).parents('.catalog-item-heading').siblings('.catalog-item-content').slideDown(250);
        }
      } else {
        $('.catalog-item-content').show();
      }
    });

    $(window).resize(function() {
     if($(window).width() > 576)
       $('.catalog-item-content').show();
    });

    $('.input-rating-wrap input').click( function(){
      starvalue = $(this).attr('value');

      for(i=0; i<=5; i++){
          if (i <= starvalue){
              $(".input-rating-stars-item-" + i).prop('checked', true);
          } else {
              $(".input-rating-stars-item-" + i).prop('checked', false);
          }
      }
    });

    $(".input-file-wrap .input-file").change(function() {
      $(this).parents('.input-file-wrap').find('.input-file-name').remove();
      $(this).parents('.input-file-wrap').find('.btn-reset').show();

        var files = $(this)[0].files;
        for (var i = 0; i < files.length; i++) {
            $(this).parents('.input-file-wrap').find(".input-file-title").after('<span class="input-file-name"></span>');
            $(this).parents('.input-file-wrap').find('.input-file-name:first-of-type').append(files[i].name);
        }
    });

    $('.btn-reset').on("click", function () {
      $(this).hide();
      $(this).parents('.input-file-wrap').find('.input-file').val('');
      $(this).parents('.input-file-wrap').find('.input-file-name').remove();
    });

    $('.personal-info-form .btn-edit').on("click", function () {
        $('#leave-review-file').parents(".personal-info-item").removeClass('disabled');
        $(this).siblings(".personal-info-item-input").focus();
    });

    $('.personal-password .btn-edit').on("click", function () {
        $('.personal-password').find('.personal-info-item').removeClass('disabled');
    });

    $('.btn-show-filter').on("click", function () {
      $('.category-params-block').addClass('show');
      $('.overlay').fadeIn();
    });

    $('.bx_filter_close').on("click", function () {
      $('.category-params-block').removeClass('show');
      $('.overlay').fadeOut();
    });

    $('.show-all-brands').siblings('.bx_filter_block').find('.bx_filter_param_label:nth-child(n+5)').hide();

    $('.show-all-brands').on("click", function () {
      $(this).siblings('.bx_filter_block').find('.bx_filter_param_label:nth-child(n+5)').slideDown();
      $(this).hide();
    });

    $('.popup-registration, .search__results').not('.popup-reminder').children('.popup-content').overlayScrollbars({
      overflowBehavior : {
        x : "hidden",
        y : "scroll"
      }
    });

    $('.sale-order-list-about-link').click(function(){
      ($(this).text() === "Подробнее о заказе") ? $(this).text("Скрыть") : $(this).text("Подробнее о заказе");
    });
});